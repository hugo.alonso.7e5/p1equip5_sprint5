using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HunterSpawner : MonoBehaviour
{
    public GameObject cubo;
    public GameObject cam;
    private GameObject cazador;
    public float tiempo;
    float next_spawn_time;
    public static bool flag;


    void Start()
    {
        //HunterSpawn();
        cazador = GameObject.Find("Hunter(Clone)");

        next_spawn_time = Time.time + 1.5f;

    }

    void Update()
    {
        /*if (cazador == null)
        {
            //HunterSpawn();
            StartCoroutine(waiter());
            cazador = GameObject.Find("Hunter(Clone)");
        }*/

        //spawnerEnemies.transform.position = plataformaSpawner.transform.position;

        if (Time.time > next_spawn_time)
        {
            if(flag == false)
            {
 //Instantiate enemy clones prefab...
                HunterSpawn();
                flag = true;
                next_spawn_time += 6.0f;
            }
           
        }
    }
    public void HunterSpawn()
    {
        //StartCoroutine(waiter());
        Instantiate(cubo, new Vector3(cam.transform.position.x - 6f, cam.transform.position.y), Quaternion.identity);
    }

    IEnumerator waiter()
    {
        yield return new WaitForSeconds(3);
        HunterSpawn();
    }

    
}
