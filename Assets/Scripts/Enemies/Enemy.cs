using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private GameObject player;   //volem aconseguir la position del player (en el transform)
    private Transform playerTransform;
    public float moveSpeed = 12.5f;


    private Rigidbody2D enemyRb;
    private Vector2 enemyMovement;
    private Vector3 actualizar;

    void Start()
    {
        player = GameObject.Find("Player");
        playerTransform = player.transform;
        enemyRb = this.GetComponent<Rigidbody2D>();
        actualizar = new Vector3(moveSpeed * Time.deltaTime, 0f, 0f);
    }

    private void FixedUpdate()
    {
        moveCharacter(enemyMovement);
    }
    void moveCharacter(Vector2 direction)
    {
        transform.position -= actualizar;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("bullet")) {
            Destroy(this.gameObject);
            Destroy(collision.gameObject); //Destruim la bala
            }

        //En impactar la bala amb el Enemic destrueix Enemy y la bala
                ; 
    }

    
}

