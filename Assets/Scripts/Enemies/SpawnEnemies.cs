using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour
{
    [SerializeField]
    public GameObject enemy;
    public GameObject spawnerEnemies;
    public GameObject plataformaSpawner; 

    float next_spawn_time;


    void Start()
    {
  
        next_spawn_time = Time.time + 1.5f;
    }


    void Update()
    {
        spawnerEnemies.transform.position = plataformaSpawner.transform.position; 

        if (Time.time > next_spawn_time)
        {
            //Instantiate enemy clones prefab...
            Spawn();

            next_spawn_time += 6.0f;
        }
    }

    private void Spawn()
    {

        Instantiate(enemy, spawnerEnemies.transform.position, transform.rotation);
    }
}
