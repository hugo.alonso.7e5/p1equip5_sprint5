using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reno : Enemy
{
    enum direcciones { stop, derecha, izquierda }

    private direcciones direccion;
    public Vector3 pop;
    public float velocidad;


    void Start()
    {
        velocidad = 0.3f;
        direccion = direcciones.derecha;
        pop = new Vector3(velocidad * Time.deltaTime, 0f, 0f);
    }


    void Update()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        transform.position -= pop;
    }

}
