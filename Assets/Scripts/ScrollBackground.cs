using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollBackground : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private Transform _threshold;

    private SpriteRenderer _spriteRenderer;
    private float _initPosition;
    private void Start()
    {
        _speed = 5f;
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _initPosition = _spriteRenderer.transform.position.x;
    }

    private void FixedUpdate()
    {
        if (_spriteRenderer.transform.position.x < _threshold.transform.position.x)
        {
            _spriteRenderer.transform.position = new Vector3(_initPosition, _spriteRenderer.transform.position.y, _spriteRenderer.transform.position.z);
        }

        else
        {
            _spriteRenderer.transform.position = new Vector3(_spriteRenderer.transform.position.x + 1 * _speed, _spriteRenderer.transform.position.y, _spriteRenderer.transform.position.z);
        }

    }


}

