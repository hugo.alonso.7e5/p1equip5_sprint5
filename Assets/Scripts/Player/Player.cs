using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class Player : MonoBehaviour
{
    public float playerSpeed = 10f;
    public float jumpPower = 18f;
    public float distance = 0;

    private Rigidbody2D rb;
    private float acceleration;

    private bool isGrounded;

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        acceleration = rb.velocity.x + playerSpeed * Time.deltaTime;
        MovePlayer();

        if (Input.GetKey(KeyCode.Space) && isGrounded== true)
        {
            Jump();
            isGrounded = false; 
        }

        
        distance += rb.velocity.x * Time.fixedDeltaTime;

    }

    private void MovePlayer()
    {
        rb.velocity = new Vector2(playerSpeed, rb.velocity.y);
        //rb.velocity = new Vector2(acceleration/2, rb.velocity.y);
    }
    private void Jump()
    {
        rb.velocity = new Vector2(acceleration, jumpPower);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == ("Ground") && isGrounded == false)
        {
            isGrounded = true;
        }

        if (col.gameObject.tag == ("enemy")) SceneManager.LoadScene("LoseScene");

    }


    /*
    private bool IsGrounded()
    {
        var groundCheck = Physics2D.Raycast(transform.position, Vector2.down, 0.7f);
        return groundCheck.collider != null && groundCheck.collider.CompareTag("Ground");
    }*/
}
