using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject bullet;
    GameObject tempBullet; 
    public Transform firePoint;
    private float bulletSpeed = 23f;
    Vector3 lookPos;
    void Update()
    {
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        lookPos = Camera.main.ScreenToWorldPoint(mousePos) - transform.position;

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            PlayerShooting();
        }

    }
    private void PlayerShooting()
    {
        tempBullet = Instantiate(bullet, firePoint.position, firePoint.rotation);
        Rigidbody2D rb = tempBullet.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.right * bulletSpeed, ForceMode2D.Impulse);
    }

    

}