using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformSpawner : MonoBehaviour
{
    public GameObject plataforma1, plataforma2, plataforma3;
    bool hasground = true;
    public static int plataformNum;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasground)
        {
            SpawnPlataform();
            hasground = true; 
        } 
    }

    public void SpawnPlataform()
    {
        //int randomNum = Random.Range(1, 4);
        //Debug.Log(randomNum);
        //int randomNum = 1;
        
        plataformNum = Random.Range(1,4); 
        
        if (plataformNum == 1) plataformNum = Random.Range(1, 4);
        if (plataformNum == 2) plataformNum = Random.Range(1, 4);
        if (plataformNum == 3) plataformNum = Random.Range(1, 4);

        Debug.Log(plataformNum); 
        if (plataformNum == 1) Instantiate(plataforma1, new Vector3(transform.position.x + 12, -3.0f, 0), Quaternion.identity);
        if (plataformNum == 2) Instantiate(plataforma2, new Vector3(transform.position.x + 12, -2.0f, 0), Quaternion.identity);
        if (plataformNum == 3) Instantiate(plataforma3, new Vector3(transform.position.x + 12, -1.0f, 0), Quaternion.identity);
    }

    private void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ground")) hasground = true;
    }

    private void OnTriggerExit2D (Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ground")) hasground = false;
    }

}


